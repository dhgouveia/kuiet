cmake_minimum_required(VERSION 3.1.0)

if(NOT CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE Debug CACHE STRING "Choose the type of build (Debug or Release)" FORCE)
endif()

if(CMAKE_VERSION VERSION_LESS "3.7.0")
    set(CMAKE_INCLUDE_CURRENT_DIR ON)
endif()

set(EXECUTABLE_NAME "Kuiet")
project(${EXECUTABLE_NAME})

set(CMAKE_CXX_STANDARD 11)
set(SRC_PATH ${CMAKE_SOURCE_DIR}/src)
set(BIN_PATH ${CMAKE_SOURCE_DIR}/build)
set(VENDOR_PATH ${CMAKE_SOURCE_DIR}/vendor)
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${BIN_PATH})
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${BIN_PATH})
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${BIN_PATH})

# Sources
set (SourceGroup_Main
	"${SRC_PATH}/MainWindow.h"
	"${SRC_PATH}/MainWindow.cpp"
	"${SRC_PATH}/main.cpp"
)
source_group("Main" FILES ${SourceGroup_Main})

set (SourceGroup_Audio
	"${SRC_PATH}/IAudioFilter.h"
	"${SRC_PATH}/AudioCapture.h"
	"${SRC_PATH}/AudioCapture.cpp"
)
source_group("Audio" FILES ${SourceGroup_Audio})

set (SourceGroup_AudioFilters
	"${SRC_PATH}/LowPassFilter.h"
	"${SRC_PATH}/RNNoiseFilter.h"
)
source_group("Audio\\Filters" FILES ${SourceGroup_AudioFilters})

set (SOURCE_FILES
	${SourceGroup_Main}
	${SourceGroup_Audio}
	${SourceGroup_AudioFilters}
)
add_executable(${EXECUTABLE_NAME} ${SOURCE_FILES})

# QT5 =====
# https://doc.qt.io/qt-5/cmake-get-started.html#build-a-gui-executable
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)
set(CMAKE_AUTOUIC ON)
# Custom Qt Path https://doc.qt.io/qt-5/cmake-get-started.html
set(Qt5_DIR "D:/Programacion/SDKs/Qt/5.15.1/5.15.1/msvc2019_64/lib/cmake/Qt5")
find_package(Qt5 COMPONENTS Core Widgets Multimedia REQUIRED)
target_link_libraries(${EXECUTABLE_NAME} Qt5::Core Qt5::Widgets Qt5::Multimedia)

#== RNNoise Lib
include_directories ("${VENDOR_PATH}/rnnoise/include")
target_link_libraries(${EXECUTABLE_NAME}
	debug "${VENDOR_PATH}/rnnoise/lib/rnnoised.lib"
	optimized "${VENDOR_PATH}/rnnoise/lib/rnnoise.lib"
)