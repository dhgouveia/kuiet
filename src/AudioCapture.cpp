#include <QDebug>
#include "AudioCapture.h"

AudioCapture::AudioCapture() :
	m_inputBuffer(BUFFER_SIZE_MSC, 0),
	m_outputDevice(nullptr),
	m_audioFilter(nullptr),
	m_inputDevice(nullptr),
	m_outputIO(nullptr),
	m_inputIO(nullptr)
{
	// RRNoise operates on RAW 16 - bit(machine endian) mono, sampled at 48 kHz.
	m_format.setSampleRate(SAMPLE_RATE);
	m_format.setChannelCount(1);
	m_format.setSampleSize(16);
	m_format.setSampleType(QAudioFormat::SignedInt);
	m_format.setByteOrder(QAudioFormat::LittleEndian);
	m_format.setCodec("audio/pcm");
}

AudioCapture::~AudioCapture() {
	if (m_audioFilter) {
		delete m_audioFilter;
		m_audioFilter = nullptr;
	}
}

void AudioCapture::setFilter(IAudioFilter* filter)
{
	if (m_audioFilter) {
		delete m_audioFilter;
	}
	m_audioFilter = filter;
}

void AudioCapture::update() {

	if (!m_inputDevice) {
		return;
	}

	qint64 bytesLen = m_inputDevice->bytesReady();

	if (bytesLen > BUFFER_SIZE) {
		bytesLen = BUFFER_SIZE;
	}

	qint64 bytesRead = m_inputIO->read(m_inputBuffer.data(), bytesLen);

	if (bytesRead > 0)
	{
		short* inBuffer = (short*)m_inputBuffer.constData();
		short* outBuffer = (short*)m_inputBuffer.data();

		if (m_audioFilter) {
			m_audioFilter->process(inBuffer, outBuffer, bytesLen);
		}

		m_outputIO->write((char*)outBuffer, bytesLen);
	}
}

void AudioCapture::start(QAudioDeviceInfo& inputDeviceInfo, QAudioDeviceInfo& outputDeviceInfo)
{
	if (m_inputIO != nullptr) {
		disconnect(m_inputIO, 0, this, 0);
		m_inputIO = nullptr;
	}
	if (m_outputIO != nullptr) {
		m_outputIO = nullptr;
	}

	// Create Input Device
	if (!inputDeviceInfo.isFormatSupported(m_format))
	{
		//Default format not supported - trying to use nearest
		qWarning() << "Input Device format not supported, trying to use the nearest.";
		m_format = inputDeviceInfo.nearestFormat(m_format);
	}
	m_inputDevice = new QAudioInput(inputDeviceInfo, m_format, this);

	// Create Output Device
	if (!outputDeviceInfo.isFormatSupported(m_format))
	{
		//Default format not supported - trying to use nearest
		qWarning() << "Output Device format not supported, trying to use the nearest.";
		m_outputFormat = outputDeviceInfo.nearestFormat(m_format);
	}
	m_outputDevice = new QAudioOutput(outputDeviceInfo, m_format, this);

	// Start
	m_inputIO = m_inputDevice->start();
	m_outputIO = m_outputDevice->start();

	connect(m_inputIO, &QIODevice::readyRead, this, &AudioCapture::update);
}

void AudioCapture::stop() {
	m_inputDevice->stop();
	m_outputDevice->stop();
}