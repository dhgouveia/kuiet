#pragma once
#include <QObject>
#include <QByteArray>
#include <QAudioInput>
#include <QAudioOutput>
#include "IAudioFilter.h"

#define BUFFER_SIZE 4096
#define BUFFER_SIZE_MSC 14096
#define SAMPLE_RATE 48000

class AudioCapture : public QObject {
public:
	AudioCapture();
	~AudioCapture();
	void setFilter(IAudioFilter* filter);
	void start(QAudioDeviceInfo& inputDeviceInfo, QAudioDeviceInfo& outputDeviceInfo);
	void stop();
private:
	void update();
private:
	QAudioFormat m_format;
	QAudioFormat m_outputFormat;
	QAudioInput* m_inputDevice;
	QAudioOutput* m_outputDevice;
	QIODevice* m_inputIO;
	QIODevice* m_outputIO;
	QByteArray m_inputBuffer;

	// this could be changed in the future
	// to a vector of IAudioFilter to apply multiple filters
	IAudioFilter* m_audioFilter;
};