#pragma once
class IAudioFilter {
public:
	virtual void process(short* inBuffer, short* outBuffer, long long bufferLen) = 0;
};