#include <iostream>
#include "MainWindow.h"

#include <QLabel>
#include <QGroupBox>
#include <QVBoxLayout>
#include <QAudioInput>
#include <QAudioOutput>

// Filters
#include "LowPassFilter.h"
#include "RNNoiseFilter.h"

MainWindow::MainWindow(QWidget* _parent)
	: QMainWindow(_parent), m_driverSelector(nullptr),
	m_inputSelector(nullptr), m_outputSelector(nullptr),
	m_audioCapture(nullptr), m_startBtn(nullptr)
{
	initWindow();
	initDevices();
}
MainWindow::~MainWindow() {
	delete m_audioCapture;
	m_audioCapture = nullptr;
}

void MainWindow::initWindow()
{
	resize(426, 480);
	setWindowTitle("Kuiet");

	// Main Layout
	QWidget* widget = new QWidget;
	QVBoxLayout* mainLayout = new QVBoxLayout;
	mainLayout->setMargin(20);
	mainLayout->setSpacing(20);
	widget->setLayout(mainLayout);
	setCentralWidget(widget);

	// Description
	QLabel* descriptionText = new QLabel(tr("<p><b>Kuiet</b> supress noise using <a href=\"https://jmvalin.ca/demo/rnnoise/\" target=\"_blank\"><span style=\"text-decoration: underline; color:#0000ff;\">RNNoise</span></a> library based on a recurrent neural network.</p><p>Note: in order to work, you will need a virtual audio input</p>"));
	descriptionText->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
	descriptionText->setOpenExternalLinks(true);
	mainLayout->addWidget(descriptionText);

	// Filter
	m_filterSelector = new QComboBox;
	QGroupBox* filterGroupBox = new QGroupBox(tr("Filter"));
	QVBoxLayout* filterLayout = new QVBoxLayout;
	filterGroupBox->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
	filterGroupBox->setLayout(filterLayout);
	filterLayout->addWidget(m_filterSelector);
	mainLayout->addWidget(filterGroupBox);
	m_filterSelector->addItem("None", QVariant::fromValue(FILTER::NONE));
	m_filterSelector->addItem("LowPass", QVariant::fromValue(FILTER::LOWPASS));
	m_filterSelector->addItem("RRNoise", QVariant::fromValue(FILTER::RNNOISE));

	connect(m_filterSelector, QOverload<int>::of(&QComboBox::activated), this, [=]() {
		if (!m_audioCapture) {
			return;
		}

		FILTER filter = m_filterSelector->currentData().value<FILTER>();

		switch (filter) {
		case FILTER::NONE:
			m_audioCapture->setFilter(nullptr);
			break;
		case FILTER::LOWPASS:
			m_audioCapture->setFilter(new LowPassFilter());
			break;
		case FILTER::RNNOISE:
			m_audioCapture->setFilter(new RNNoiseFilter());
			break;
		}
		});

	// Device Selectors
	QGroupBox* devicesGroupBox = new QGroupBox(tr("Devices"));
	QGroupBox* driverGroupBox = new QGroupBox(tr("Audio Driver"));
	QGroupBox* inputGroupBox = new QGroupBox(tr("Input"));
	QGroupBox* outputGroupBox = new QGroupBox(tr("Output"));

	QVBoxLayout* devicesLayout = new QVBoxLayout;
	devicesGroupBox->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
	devicesGroupBox->setLayout(devicesLayout);

	devicesLayout->addWidget(driverGroupBox);
	devicesLayout->addWidget(inputGroupBox);
	devicesLayout->addWidget(outputGroupBox);
	mainLayout->addWidget(devicesGroupBox);

	// Driver
	m_driverSelector = new QComboBox;
	QVBoxLayout* driverLayout = new QVBoxLayout;
	driverGroupBox->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
	driverGroupBox->setLayout(driverLayout);
	driverLayout->addWidget(m_driverSelector);
	connect(m_driverSelector, QOverload<int>::of(&QComboBox::activated), this, [=]() {
		m_driverSelectedName = m_driverSelector->currentData().toString();
		updateInputSelector();
		updateOutputSelector();
		});

	// Input
	m_inputSelector = new QComboBox;
	QVBoxLayout* inputLayout = new QVBoxLayout;
	inputGroupBox->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
	inputGroupBox->setLayout(inputLayout);
	inputLayout->addWidget(m_inputSelector);

	// Output
	m_outputSelector = new QComboBox;
	QVBoxLayout* outputLayout = new QVBoxLayout;
	outputGroupBox->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
	outputGroupBox->setLayout(outputLayout);
	outputLayout->addWidget(m_outputSelector);

	// Activate button
	QWidget* startWidget = new QWidget;
	QVBoxLayout* startLayout = new QVBoxLayout;
	m_startBtn = new QPushButton("Start");
	startLayout->setAlignment(Qt::AlignHCenter);
	startLayout->addWidget(m_startBtn);
	startWidget->setLayout(startLayout);
	mainLayout->addStretch(0);
	mainLayout->addWidget(startWidget);

	connect(m_startBtn, &QPushButton::released, this, &MainWindow::toggleCapture);
}

void MainWindow::toggleCapture()
{
	m_bIsActive = !m_bIsActive;
	m_startBtn->setText(m_bIsActive ? "Stop" : "Start");
	if (m_audioCapture != nullptr) {
		if (m_bIsActive) {
			m_audioCapture->start(
				m_inputSelector->currentData().value<QAudioDeviceInfo>(),
				m_outputSelector->currentData().value<QAudioDeviceInfo>()
			);
		}
		else {
			m_audioCapture->stop();
		}

		// m_filterSelector->setDisabled(m_bIsActive);
		m_driverSelector->setDisabled(m_bIsActive);
		m_inputSelector->setDisabled(m_bIsActive);
		m_outputSelector->setDisabled(m_bIsActive);
	}
}

void MainWindow::initDevices()
{
	m_audioCapture = new AudioCapture();

	QList<QAudioDeviceInfo> inputList = QAudioDeviceInfo::availableDevices(QAudio::AudioInput);
	QList<QAudioDeviceInfo> outputList = QAudioDeviceInfo::availableDevices(QAudio::AudioOutput);

	// the first items are the default ones
	m_driverSelectedName = inputList[0].realm();
	m_defaultInputName = inputList[0].deviceName();
	m_defaultOutputName = outputList[0].deviceName();

	QString currentRealm = m_driverSelectedName;
	m_driverSelector->addItem(currentRealm, currentRealm);

	for (auto& deviceInfo : inputList)
	{
		const QString& deviceRealm = deviceInfo.realm();

		// Group the devices by "driver" ex: wasapi, alsa
		m_devicesInputList[deviceRealm].push_back(QVariant::fromValue(deviceInfo));

		// Add the driver into de list
		if (currentRealm != deviceRealm)
		{
			m_driverSelector->addItem(deviceRealm, deviceRealm);
			currentRealm = deviceRealm;
		}
	}

	for (auto& deviceInfo : outputList)
	{
		m_devicesOutputList[deviceInfo.realm()].push_back(QVariant::fromValue(deviceInfo));
	}

	updateInputSelector();
	updateOutputSelector();
}

void MainWindow::updateInputSelector()
{
	m_inputSelector->clear();
	for (QVariant item : m_devicesInputList[m_driverSelectedName]) {
		QAudioDeviceInfo deviceInfo = item.value<QAudioDeviceInfo>();
		const QString& deviceName = deviceInfo.deviceName();
		m_inputSelector->addItem(deviceName, item);
		if (m_defaultInputName == deviceName) {
			m_inputSelector->setCurrentIndex(m_inputSelector->count() - 1);
		}
	}
}

void MainWindow::updateOutputSelector()
{
	m_outputSelector->clear();
	for (QVariant item : m_devicesOutputList[m_driverSelectedName]) {
		QAudioDeviceInfo deviceInfo = item.value<QAudioDeviceInfo>();
		const QString& deviceName = deviceInfo.deviceName();
		m_outputSelector->addItem(deviceName, item);
		if (m_defaultOutputName == deviceName) {
			m_outputSelector->setCurrentIndex(m_outputSelector->count() - 1);
		}
	}
}