#include "IAudioFilter.h"
#include "rnnoise.h"
#include <cmath>

#define FRAME_SIZE 480

class RNNoiseFilter : public IAudioFilter {
public:
	RNNoiseFilter() : st(rnnoise_create(NULL)) {}
	~RNNoiseFilter() { rnnoise_destroy(st); }
	void process(short* inBuffer, short* outBuffer, long long bufferLen) {
		int chunkCount = std::ceil(bufferLen / FRAME_SIZE);
		for (int i = 0; i <= chunkCount; i++) {
			for (int j = 0; j < FRAME_SIZE; j++) {
				int frameIndex = (i * FRAME_SIZE) + j;
				rrnoise_inBuffer[j] = inBuffer[frameIndex];
			}

			rnnoise_process_frame(st, rrnoise_outBuffer, rrnoise_inBuffer);

			for (int j = 0; j < FRAME_SIZE; j++) {
				int frameIndex = (i * FRAME_SIZE) + j;
				outBuffer[frameIndex] = rrnoise_outBuffer[j];
			}
		}
	}
private:
	DenoiseState* st;
	float rrnoise_inBuffer[FRAME_SIZE] = { 0 };
	float rrnoise_outBuffer[FRAME_SIZE] = { 0 };
};