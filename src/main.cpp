#include <QtWidgets/QApplication>
#include "MainWindow.h"

int main(int argc, char* argv[])
{
	QApplication app(argc, argv);
	app.setApplicationName("Kuiet");

	MainWindow* mainWindow = new MainWindow();
	mainWindow->show();
	return app.exec();
}