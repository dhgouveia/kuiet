#include "IAudioFilter.h"
class LowPassFilter :public IAudioFilter {
public:
	LowPassFilter() = default;
	void process(short* inBuffer, short* outBuffer, long long bufferLen) {
		for (int i = 1; i < bufferLen; i++)
		{
			outBuffer[i] = 0.333 * inBuffer[i] + (1.0 - 0.333) * outBuffer[i - 1];
		}
	}
};