#pragma once
#include <QComboBox>
#include <QMainWindow>
#include <QPushButton>
#include "AudioCapture.h"

class MainWindow : public QMainWindow
{
public:
	MainWindow(QWidget* _parent = nullptr);
	~MainWindow();
private:
	void initWindow();
	void initDevices();
	void updateInputSelector();
	void updateOutputSelector();
	void toggleCapture();
public:
	enum class FILTER {
		NONE = 0,
		LOWPASS,
		RNNOISE
	};
private:
	// QT Specific
	QPushButton* m_startBtn;
	QString m_driverSelectedName;
	QString m_defaultInputName;
	QString m_defaultOutputName;
	QComboBox* m_driverSelector;
	QComboBox* m_inputSelector;
	QComboBox* m_outputSelector;
	QComboBox* m_filterSelector;
	std::map<QString, std::vector<QVariant>> m_devicesInputList;
	std::map<QString, std::vector<QVariant>> m_devicesOutputList;

	AudioCapture* m_audioCapture;
	bool m_bIsActive = false;
};

Q_DECLARE_METATYPE(MainWindow::FILTER)