# Kuiet

Cross-platform app similar to [RTXVoice](https://www.nvidia.com/en-us/geforce/guides/nvidia-rtx-voice-setup-guide/) using [RNNoise](https://github.com/xiph/rnnoise/) noise suppression library based on a recurrent neural network, with the help of VirtualCables (ex: [VB-Cable](https://vb-audio.com/Cable/index.htm) from vb-audio for windows/mac or [jack audio](https://jackaudio.org/) on cross-platform )

## Build

Use cmake to generate the solution and compile

### Library Dependencies

- [RNNoise](https://github.com/xiph/rnnoise/)
- [Qt 5.15.1 (LTS)](https://www.qt.io/download)


### Help Wanted

For now this code only be tested on windows

- Make sure compile on Linux
- Make sure compile on Mac

### Contributions Feature Ideas

- Built-in Virtual Input
- Posibility to stack multiple filters?
	- More filters ex: EchoCancellationFilter, CompressorFilter
	- Load external user created filters from dll?
